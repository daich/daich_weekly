import json
import pandas as pd
import numpy as np
from tqdm import *
import tqdm

import hoomd
import hoomd.md


def kinetic_energy(vel, mass):
    vel_sq = np.sum(np.square(vel), axis=1)
    kinetic = 0.5 * mass * vel_sq
    return kinetic

def potential_energy(lj, N):
    lst = []
    for i in range(N):
        lst.append(lj.forces[i].energy)
    return np.array(lst)

def get_energy(traj_fn, statepoint, N, frame):
    diam = 0.48
    T = diam * statepoint['act_mag'] / statepoint['Pe']
    mass = statepoint['gamma']**2 * diam * statepoint['x_m'] / statepoint['act_mag']

    hoomd.context.initialize('')
    system = hoomd.init.read_gsd(traj_fn, frame=frame)
    nl = hoomd.md.nlist.cell()
    all = hoomd.group.all()

    # Interaction.
    lj = hoomd.md.pair.lj(r_cut=3.0, nlist=nl)
    lj.set_params(mode="shift")
    lj.pair_coeff.set('A', 'A',
                      epsilon=statepoint['epsilon'],
                      sigma=diam,
                      r_cut = diam*2**(1./6.))
    lj.enable()

    # Integrator.
    hoomd.md.integrate.mode_standard(dt=0.00001);
    dynamics = hoomd.md.integrate.langevin(kT=T,
                                           group=all,
                                           seed=statepoint['random_seed'],
                                           noiseless_t=statepoint['noiseless_t'],
                                           noiseless_r=statepoint['noiseless_r'])
    dynamics.set_gamma('A', statepoint['gamma'])

    
    
    # Run and summary.
    hoomd.run(1)
    snapshot = system.take_snapshot()
    potential = potential_energy(lj, N)
    kinetic = kinetic_energy(snapshot.particles.velocity, mass)
    return kinetic, potential
    