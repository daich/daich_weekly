import click
import json
from tqdm import *

import gsd.hoomd
import pandas as pd

import energy_time_series

PATH = '/nfs/glotzerCBES/active_rod/disk_dynamics_prod5_finite_size'

def get_statepoint(fn):
    with open(fn, 'r') as json_file:
        statepoint = json.load(json_file)
    return statepoint

def driver_energy_calc(traj_fn, statepoint, min_frame=0,  max_frame=-1, step=1, tqdm_mode=None):
    traj = gsd.hoomd.open(name=traj_fn, mode='rb')
    if max_frame == -1:
        max_frame = len(traj)

    N = statepoint['nside'] ** 2
    df = pd.DataFrame()
    df['frame'] = [frame for frame in range(min_frame, max_frame, step)]
    df['kinetic_energy'] = [frame for frame in range(min_frame, max_frame, step)]
    df['potential_energy'] = [frame for frame in range(min_frame, max_frame, step)]
    
    # This step ensures numpy objects or lists can be stored in pandas cells.
    df = df.astype(object)

    iterator = range(len(df))
    if tqdm_mode == 'ipynb':
        iterator = tqdm_notebook(iterator)
    if tqdm_mode == 'cli':
        iterator = tqdm(iterator)

    for row_idx in iterator:
        frame = int(df.frame.iloc[row_idx])
        kinetic_energy, potential_energy = \
            energy_time_series.get_energy(traj_fn, statepoint, N, frame)
        df.at[row_idx, 'kinetic_energy'] = kinetic_energy.tolist()
        df.at[row_idx, 'potential_energy'] = potential_energy.tolist()
    return df

@click.command()
@click.option('--workspace',
              required=True,
              prompt='Workspace')
@click.option('--signac_id',
              required=True,
              prompt='Signac id')
@click.option('--outpath',
              required=True,
              prompt='Output file path, without filename')
def exe(workspace, signac_id, outpath):
    fn = PATH + '/' + workspace + '/' + '{}/video.gsd'.format(signac_id)
    sp_fn = fn.replace('video.gsd', 'signac_statepoint.json')
    statepoint = get_statepoint(sp_fn)

    df = driver_energy_calc(fn, statepoint)
    df.to_hdf('{}/energy_{}.h5'.format(outpath, signac_id), 'df')


if __name__ == '__main__':
    exe()
