import click
from tqdm import *

import gsd.hoomd
import pandas as pd

import neighbor_time_series
from neighbor_time_series import neighbor_time_series_stat


PATH = '/nfs/glotzerCBES/active_rod/disk_dynamics_prod5_finite_size'


@click.command()
@click.option('--workspace',
              required=True,
              prompt='Workspace')
@click.option('--signac_id',
              required=True,
              prompt='Signac id')
@click.option('--outpath',
              required=True,
              prompt='Output file path, without filename')
def exe(workspace, signac_id, outpath):
    fn = PATH + '/' + workspace + '/' + '{}/video.gsd'.format(signac_id)
    boxfile = fn.replace('video.gsd', 'signac_job_document.json')

    traj = gsd.hoomd.open(name=fn, mode='rb')
    box = neighbor_time_series.get_box(boxfile)
    print(len(traj))
    
    df = neighbor_time_series.neighbor_time_series_stat(box, traj, min_frame=0,  max_frame=-1, step=1, tqdm_mode=None)
    df.to_hdf('{}/{}.h5'.format(outpath, signac_id), 'df')


if __name__ == '__main__':
    exe()
