import json
import pandas as pd
import numpy as np
from tqdm import *
import tqdm

import freud
from freud.locality import NearestNeighbors
from freud.density import LocalDensity


class Box(object):
    def __init__(self, lx, ly):
        self.Lx = lx
        self.Ly = ly
        self.Lz = 0
        self.xy = 0
        self.xz = 0
        self.yz = 0
        self.dimensions = 2

def get_box(boxfile):
    with open(boxfile,'r') as jsonf:
        meta = (json.load(jsonf))['hoomd_meta']['hoomd_data_system_data']
    lx = meta['box']['Lx']
    ly = meta['box']['Ly']
    box = Box(lx, ly)
    box = freud.box.Box.from_box(box)
    return box

def get_neigh(box, pos):
    diam = 0.48
    r_cut = diam * 2**(1/6)
    nn = NearestNeighbors(r_cut, 6, strict_cut=True)
    nn.compute(box, pos, pos)
    nl = nn.getNeighborList()
    iui32 = np.iinfo(np.uint32)
    return nl

def get_num_neigh(box, pos):
    diam = 0.48
    r_cut = diam * 2**(1/6)
    nn = NearestNeighbors(r_cut, 6, strict_cut=True)
    nn.compute(box, pos, pos)
    nl = nn.getNeighborList()
    iui32 = np.iinfo(np.uint32)
    return np.sum(nl != iui32.max, axis=1)

def get_local_density(box, pos):
    diam = 0.48
    radius = diam / 2
    volume = np.pi * radius ** 2
    r_cut = diam * 4 * 2**(1/6)
    density_calculator = LocalDensity(r_cut, volume, diam)
    density_calculator.compute(box, pos, pos)
    return density_calculator.density

def neighbor_time_series_stat(box, traj, min_frame, max_frame, step, tqdm_mode=None):
    if max_frame == -1:
        max_frame = len(traj)

    df = pd.DataFrame()
    df['frame'] = [frame for frame in range(min_frame, max_frame, step)]

    iterator = range(len(df))
    if tqdm_mode == 'ipynb':
        iterator = tqdm_notebook(iterator)
    if tqdm_mode == 'cli':
        iterator = tqdm(iterator)

    for row_idx in iterator:
        frame = int(df.frame.iloc[row_idx])
        num_neigh = get_num_neigh(box, traj[frame].particles.position)
        for i in range(7):
            df.at[row_idx, '{}_neigh'.format(i)] = np.count_nonzero(num_neigh==i) / len(num_neigh)
    return df
