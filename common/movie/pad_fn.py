import os
path = input("Input path for the confirmation")

for filename in os.listdir(path):
    num = filename[:-4]
    num = num.zfill(4)
    new_filename = num + filename[-4:]
    os.rename(os.path.join(path, filename), os.path.join(path, new_filename))
