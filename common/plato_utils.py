""" Copied from https://bitbucket.org/glotzer/platoviz/src/master/examples/jupyter_helpers.py
    Aug, 22, 2018
"""


import subprocess
import IPython, IPython.display
try:
    import PIL, PIL.Image
except ImportError:
    PIL = None
import io

def render_inplace(canvas, fname=None, process_events=False):
    """Render and display an image inline. This function is useful when
    using a non-webGL vispy backend (i.e. vispy.app.use('PySide') and
    '%gui qt4' ipython magic)"""
    if PIL is None:
        raise RuntimeError('PIL python library is required for render_inplace')

    if process_events:
        vispy.app.process_events()

    image = PIL.Image.fromarray(canvas.render(), 'RGBA')
    if fname:
        image.save(fname, format='png')
        disp = IPython.display.Image(filename=fname, format='png')
    else:
        buf = io.BytesIO()
        image.save(buf, format='png')
        disp = IPython.display.Image(data=buf.getvalue(), format='png')
    return disp

def render_povray_and_display(contents, target, width, height, antialiasing=None, threads=None):
    """Renders a string using povray.

    :param contents: povray-format string to render
    :param target: Target (png) file to render to
    :param width: width of the image
    :param height: height of the image
    :param antialiasing: Control antialiasing (None->disable; value is passed in as povray's antialiasing threshold)
    :param threads: number of threads to use
    """
    povfile = target + '.pov'
    with open(povfile, 'w') as f:
        f.write(contents)

    command = ['povray', '+I{}'.format(povfile), '+O{}'.format(target),
               '+W{}'.format(width), '+H{}'.format(height)]

    if antialiasing:
        command.append('+A{}'.format(antialiasing))

    if threads:
        command.append('+WT{}'.format(threads))

    subprocess.check_call(command)
    return IPython.display.Image(filename=target)
