import json


def remove_const_key(list_of_dicts):
    unique_values = {key: set() for key in list_of_dicts[0]}
    for dic in list_of_dicts:
        for key in dic:
            unique_values[key].add(dic[key])
    const_keys = [key for key in unique_values if len(unique_values[key]) == 1]
    var_keys = [key for key in unique_values if len(unique_values[key]) > 1]
    
    def slice(dic, var_keys):
        return {key: dic[key] for key in dic if key in var_keys}

    return [slice(dic, var_keys) for dic in list_of_dicts]

def get_statepoint(fn):
    with open(fn, 'r') as json_file:
        statepoint = json.load(json_file)
    return statepoint

